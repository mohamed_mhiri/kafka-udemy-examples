package kafka.examples.controllers;

import java.util.Properties;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.StringBuilder;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.atomic.AtomicLong;

import kafka.examples.models.Response;

@RestController
public class ConsumerController {

  private final AtomicLong counter = new AtomicLong();

  @RequestMapping("/consumer")
  public Response read() {
    Properties props = new Properties();
    props.setProperty("bootstrap.servers", "127.0.0.1:9092");
    props.setProperty("key.deserializer", StringDeserializer.class.getName());
    props.setProperty("value.deserializer", StringDeserializer.class.getName());
    props.setProperty("group.id", "test");
    // by default is true
    props.setProperty("enable.auto.commit", "true");
    // every second offset will be commited
    props.setProperty("auto.commit.interval.ms", "1000");
    props.setProperty("auto.offset.reset", "earliest");
    KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
    consumer.subscribe(Arrays.asList("second_topic"));
    List<String> data = new ArrayList<>();
    int i = 0;
    while(i < 10) {
      ConsumerRecords<String, String> consumerRecords = consumer.poll(100);
      for (ConsumerRecord<String,String> consumerRecord : consumerRecords) {
        /*
        consumerRecord.value();
        consumerRecord.key();
        consumerRecord.offset();
        consumerRecord.partition();
        consumerRecord.topic();
        consumerRecord.timestamp();
        */
        StringBuilder str = new StringBuilder();
        str.append("Partition: ")
           .append(consumerRecord.partition())
           .append(", Offset: ")
           .append(consumerRecord.offset())
           .append(", Key: ")
           .append(consumerRecord.key())
           .append(", Value: ")
           .append(consumerRecord.value());
        System.out.println(str.toString());
        data.add(str.toString());
      }
      i++;
    }
    return new Response(counter.incrementAndGet(), String.join(", ", data));
  }
}
