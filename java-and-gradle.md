# INSTALL JAVA AND GRADLE

1. java(1.8) and gradle(4.7)
  1. follow these links to install java on
      * ubuntu

        [How To Install Java with Apt-Get on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04)
        * fedora or centOS

        [How To Install Java on CentOS and Fedora](https://www.digitalocean.com/community/tutorials/how-to-install-java-on-centos-and-fedora)
        * Windows

        [How do I manually download and install Java for my Windows computer](https://java.com/en/download/help/windows_manual_download.xml)

        Then you need to

        [Set JAVA_HOME variable](https://confluence.atlassian.com/doc/setting-the-java_home-variable-in-windows-8895.html)
        * MacOS

        [Install Java on Mac using brew](https://stackoverflow.com/questions/24342886/how-to-install-java-8-on-mac)

  2. install gradle on
          * Unix like systems use SDKMAN to make it easy

              [Install SDKMAN](http://sdkman.io/install.html)
          * Windows use CHOCOLATEY to simplify it

              [Install CHOCOLATEY](https://chocolatey.org/install)

            Finally, follow this link

              [Install Gradle](https://gradle.org/install/)

   To test that everything is ok run:
```
> java -version

java version "1.8.0_171"

Java(TM) SE Runtime Environment (build 1.8.0_171-b11)

Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)

> gradle -version


------------------------------------------------------------
Gradle 4.7
------------------------------------------------------------

Build time:   2018-04-18 09:09:12 UTC

Revision:     b9a962bf70638332300e7f810689cb2febbd4a6c

Groovy:       2.4.12

Ant:          Apache Ant(TM) version 1.9.9 compiled on February 2 2017

JVM:          1.8.0_171 (Oracle Corporation 25.171-b11)

OS:           Linux 4.13.0-37-generic amd64
```
2. java ide or text editor with a terminal / command prompt

Now you are ready by Allah willing to build our fix project and run it:
```
gradle wrapper --gradle-version 4.7

./gradlew build

java -jar ./build/libs/kafka-examples-0.1.0.jar

```
