package kafka.examples.controllers;

import java.util.Properties;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.atomic.AtomicLong;

import kafka.examples.models.Response;

@RestController
public class ProducerController {

  private final AtomicLong counter = new AtomicLong();

  @RequestMapping("/producer")
  public Response produce(@RequestParam(value="data", defaultValue="World") String data) {
    Properties props = new Properties();
    props.setProperty("bootstrap.servers", "127.0.0.1:9092");
    props.setProperty("key.serializer", StringSerializer.class.getName());
    props.setProperty("value.serializer", StringSerializer.class.getName());
    props.setProperty("acks", "1");
    props.setProperty("retries", "3");
    props.setProperty("linger.ms", "1");

    Producer<String, String> producer = new KafkaProducer<String, String>(props);
    ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>("second_topic", "3", data);
    producer.send(producerRecord);
    producer.close();
    return new Response(counter.incrementAndGet(), data);
  }
}
