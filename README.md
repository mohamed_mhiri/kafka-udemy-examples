# Kafka Examples

## Kafka Producer basic REST Api

## Kafka Consumer basic REST Api

### Requirements:

[Install Java and Gradle](java-and-gradle.md)

[Install Apache Kafka](apache-kafka.md)

### Create the Producer Api

[Producer REST API](producer.md)

### Create the Consumer Api

[Consumer REST API](consumer.md)
