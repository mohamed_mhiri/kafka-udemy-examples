# Producer REST API
1. Let's start with a basic REST API by following these guides:

[Building Java Projects with Gradle](https://spring.io/guides/gs/gradle/)

[Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
2. Create the ProducerConsumer

```
@RestController
public class ProducerController {

  ...

  @RequestMapping("/producer")
  public Response produce(
    @RequestParam(value="data", defaultValue="World") String data) {

    ...

  }
}

```

Here is the [Response Class Code](src/main/java/kafka/examples/models/Response.java)
3. Create a new Properties Object

and add properties as following

```
props.setProperty("bootstrap.servers", "127.0.0.1:9092");
props.setProperty("key.serializer", StringSerializer.class.getName());
props.setProperty("value.serializer", StringSerializer.class.getName());
props.setProperty("acks", "1");
props.setProperty("retries", "3");
props.setProperty("linger.ms", "1");
```
4. Create a KafkaProducer

```
Producer<String, String> producer = new KafkaProducer<String, String>(props);
```
5. Then a ProducerRecord

```
ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>("topic_name", "3", data);
```

3 is the key and data is the value

finally we send the producer record and close the connection with

```
producer.send(producerRecord);
producer.close();
```
