# Consumer REST API

1. Create the ConsumerController

```
@RestController
public class ConsumerController {

  ...

  @RequestMapping("/consumer")
  public Response read() {

    ...

  }
}

```

Here is the [Response Class Code](src/main/java/kafka/examples/models/Response.java)

2. Create a new Properties Object

and add properties as following

```
props.setProperty("bootstrap.servers", "127.0.0.1:9092");
props.setProperty("key.deserializer", StringDeserializer.class.getName());
props.setProperty("value.deserializer", StringDeserializer.class.getName());
props.setProperty("group.id", "test");
// by default is true
props.setProperty("enable.auto.commit", "true");
// every second offset will be commited
props.setProperty("auto.commit.interval.ms", "1000");
props.setProperty("auto.offset.reset", "earliest");
```
3. Create a KafkaConsumer

```
KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
```
4. Subscribe the consumer to a list of topics

```
consumer.subscribe(Arrays.asList("second_topic"));
```
4. Thus we can fetch the messages sent by the producer

```
List<String> data = new ArrayList<>();
int i = 0;
while(i < 10) {
  ConsumerRecords<String, String> consumerRecords = consumer.poll(100);
  for (ConsumerRecord<String,String> consumerRecord : consumerRecords) {
    StringBuilder str = new StringBuilder();
    str.append("Partition: ")
       .append(consumerRecord.partition())
       .append(", Offset: ")
       .append(consumerRecord.offset())
       .append(", Key: ")
       .append(consumerRecord.key())
       .append(", Value: ")
       .append(consumerRecord.value());
    System.out.println(str.toString());
    data.add(str.toString());
  }
  i++;
}
```

3 is the key and data is the value

finally we send the producer record and close the connection with

```
producer.send(producerRecord);
producer.close();
```
